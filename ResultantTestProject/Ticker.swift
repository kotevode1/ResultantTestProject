//
//  Ticker.swift
//  ResultantTestProject
//
//  Created by Mark on 11.04.2018.
//  Copyright © 2018 Mark. All rights reserved.
//

import Foundation
import ObjectMapper

class Ticker: Mappable {
    var stocks: [Stock]?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        stocks <- map["stock"]
    }
}

class Stock: Mappable {
    var name: String?
    var price: Price?
    var percentChange: Double?
    var volume: UInt?
    var symbol: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        name <- map["name"]
        price <- map["price"]
        percentChange <- map["percent_change"]
        volume <- map["volume"]
        symbol <- map["symbol"]
    }
}

class Price: Mappable {
    var currency: String?
    var amount: Double?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        currency <- map["currency"]
        amount <- map["amount"]
    }
}
