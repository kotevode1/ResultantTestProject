//
//  StocksAPI.swift
//  ResultantTestProject
//
//  Created by Mark on 11.04.2018.
//  Copyright © 2018 Mark. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class StocksAPI {
    
    private struct Constants {
        static let TickerURL = "http://phisix-api3.appspot.com/stocks.json"
    }
    
    static func fetch() -> Observable
    
}
