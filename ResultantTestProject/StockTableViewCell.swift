//
//  StockTableViewCell.swift
//  ResultantTestProject
//
//  Created by Mark on 11.04.2018.
//  Copyright © 2018 Mark. All rights reserved.
//

import UIKit

class StockTableViewCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    
    var stock: Stock? {
        didSet {
            guard stock != nil else { return }
            nameLabel.text = stock!.name
            priceLabel.text = String(format: "%.2f %@",
                                     stock!.price!.amount!,
                                     stock!.price!.currency!)
            valueLabel.text = "\(stock!.volume!) \(stock!.symbol!)"
        }
    }
    
}
