
//
//  StocksTableViewController.swift
//  ResultantTestProject
//
//  Created by Mark on 11.04.2018.
//  Copyright © 2018 Mark. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class StocksTableViewController: UITableViewController {
    
    private struct Constants {
        static let RefreshInterval = 15.0
        static let CellIdenitifer = "StockCell"
        static let TickerURL = "http://phisix-api3.appspot.com/stocks.json"
    }
    
    var timer: Timer?
    var stocks: [Stock] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    var loading = false
    
    // MARK: - Actions
    
    @IBAction func forceRefresh(_ sender: Any) {
        refresh()
    }
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        timer = Timer.scheduledTimer(withTimeInterval: Constants.RefreshInterval,
                                     repeats: true,
                                     block: { (_) in
            self.refresh()
        })
        refresh()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        timer?.invalidate()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stocks.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdenitifer, for: indexPath) as! StockTableViewCell
        cell.stock = stocks[indexPath.row]
        return cell
    }
    
    // MARK: - Private

    private func refresh() {
        guard !loading else { return }
        loading = true
        Alamofire.request(Constants.TickerURL)
            .validate()
            .responseObject { [weak self] (response: DataResponse<Ticker>) in
                self?.loading = false
                switch response.result {
                case .success(let value):
                    self?.stocks = value.stocks!
                case .failure(let error):
                    debugPrint(error.localizedDescription)
                }
        }
    }
    
}
